from django.contrib import admin
from .forms import RegModelForm
from .models import Registrado

class AdminRegistrado(admin.ModelAdmin):
    list_display = ["nombre", "email", "fecha"]
    form = RegModelForm
    list_editable = ["email"]
    list_filter = ["fecha"]
    search_fields = ["nombre", "email"]

    # class Meta:
    #    model = Registrado

# Register your models here.
admin.site.register(Registrado, AdminRegistrado)
