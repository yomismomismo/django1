from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from .forms import RegModelForm, ContactForm  # Importamos el formulario RegForm desde esta ruta './forms'
from .models import Registrado


# Create your views here.
def inicio(request):
    titulo = "BIENVENIDOS A NUESTRA PRIMERA APLICACION WEB DJANGO 1.11"
    # variable1 = "Enhorabuena!"
    form = RegModelForm(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" %(request.user)

    contexto = {
        "el_titulo": titulo,
        "el_formulario": form,
    }

    if form.is_valid():
        instance = form.save(commit=False)
        if not instance.nombre:
            instance.nombre = "PERSONA NN"
        instance.save() # CON ESTA SENTENCIA SE CREAN LOS OBJETOS EN LA BASE DE DATOS

        contexto = {
            "el_titulo": "Gracias %s!" %(instance.nombre)
        }

        print(instance)
        # form_data = form.cleaned_data
        # nombre2 = form_data.get("nombre")
        # email2 = form_data.get("email")
        # objeto = Registrado.objects.create(nombre=nombre2, email=email2)

        # OTRA FORMA DE GUARDAR LOS OBJETOS:
        #objeto = Registrado()
        #objeto.nombre = nombre2
        #objeto.email = email2
        #objeto.save()

    return render(request, "inicio.html", contexto) # Cambiamos {} por el nombre del diccionario creado 'contexto'
    #return render(request, "base.html", contexto) # Cambiamos {} por el nombre del diccionario creado 'contexto'


def contacto(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():

        # print(form.cleaned_data)

        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_email = form.cleaned_data.get("email")
        formulario_mensaje = form.cleaned_data.get("mensaje")
        asunto = "Formulario de contacto web"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "jorgeborquez@udec.cl"]
        email_mensaje = "Enviado por %s - Correo: %s - Mensaje: %s" %(formulario_nombre, formulario_email, formulario_mensaje)
        send_mail(asunto,
                  email_mensaje,
                  email_from,
                  email_to,
                  fail_silently=False
                  )
        # print(nombre, email, mensaje)

        # for key in form.cleaned_data:
        #     print(key)
        #     print(form.cleaned_data.get(key))

        # for key, value in form.cleaned_data.items():
        #     print(key, value)

    contexto = {
        "el_contacto": form,
    }
    return render(request, "contacto.html", contexto)
