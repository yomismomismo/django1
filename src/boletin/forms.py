from django import forms
from .models import Registrado

class RegModelForm(forms.ModelForm):
    class Meta:
        model = Registrado
        campos = ["nombre", "email"]
        exclude = ()

    # def clean_nombre(self):
    #     nombre = self.cleaned_data.get("nombre")
    #     if nombre: # Validamos que el contenido del campo nombre no venga vacío
    #         if len(nombre) <= 1: # Verificamos que el contenido del campo nombre tiene un caracter
    #             raise forms.ValidationError("El campo nombre no puede ser de un caracter")
    #         return nombre
    #     else:
    #         raise forms.ValidationError("Debe ingresar al menos un caracter")


    def clean_email(self):
        email = self.cleaned_data.get("email")  # Obtenermos la información del correo
        email_base, proveedor = email.split("@")  # Separamos el correo hasta la arroba
        dominio, extension = proveedor.split(".")  # Separamos el correo con la extensión
        if not extension == "edu":  # Validamos que luego del '.' deba venir la extensión EDU
            raise forms.ValidationError("Solo se aceptan correos con extensión .EDU")
        return email



class ContactForm(forms.Form):
    nombre = forms.CharField(required=False) # Este campo no será obligatorio para el formulario
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)


